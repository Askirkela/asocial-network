[![Netlify Status](https://api.netlify.com/api/v1/badges/2979a3da-cc4d-4641-b4c5-ed598fb081d1/deploy-status)](https://app.netlify.com/sites/wizardly-austin-e0982c/deploys)
# asocial

## What is this?
A little app that emulates social media apps.

Use it as a detox (more to come?).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
